package com.phani.vendorservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringCloudVendorServiceLbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudVendorServiceLbcApplication.class, args);
	}

}
